<?php

namespace Foodsharing\Modules\Blog;

use Foodsharing\Lib\Db\Db;
use Foodsharing\Modules\Bell\BellGateway;
use Foodsharing\Modules\Foodsaver\FoodsaverGateway;
use Foodsharing\Services\SanitizerService;

class BlogModel extends Db
{
	private $bellGateway;
	private $foodsaverGateway;
	private $sanitizerService;

	public function __construct(
		BellGateway $bellGateway,
		FoodsaverGateway $foodsaverGateway,
		SanitizerService $sanitizerService
	) {
		parent::__construct();
		$this->bellGateway = $bellGateway;
		$this->foodsaverGateway = $foodsaverGateway;
		$this->sanitizerService = $sanitizerService;
	}

	public function canEdit($article_id)
	{
		if ($val = $this->getValues(array('bezirk_id', 'foodsaver_id'), 'blog_entry', $article_id)) {
			if ($this->session->id() == $val['foodsaver_id'] || $this->session->isAdminFor($val['bezirk_id'])) {
				return true;
			}
		}

		return false;
	}

	public function canAdd($fsId, $bezirkId)
	{
		if ($this->session->isOrgaTeam()) {
			return true;
		}

		if ($this->session->isAdminFor($bezirkId)) {
			return true;
		}

		return false;
	}

	public function getPost($id)
	{
		return $this->qRow('
			SELECT
				b.`id`,
				b.`name`,
				b.`time`,
				UNIX_TIMESTAMP(b.`time`) AS time_ts,
				b.`body`,
				b.`time`,
				b.`picture`,
				CONCAT(fs.name," ",fs.nachname) AS fs_name
	
			FROM
				`fs_blog_entry` b,
				`fs_foodsaver` fs
	
			WHERE
				b.foodsaver_id = fs.id
	
			AND
				b.`active` = 1
	
			AND
				b.id = ' . (int)$id);
	}

	public function listNews($page)
	{
		$page = ((int)$page - 1) * 10;

		return $this->q('
			SELECT 	 	
				b.`id`,
				b.`name`,
				b.`time`,
				UNIX_TIMESTAMP(b.`time`) AS time_ts,
				b.`active`,
				b.`teaser`,
				b.`time`,
				b.`picture`,
				CONCAT(fs.name," ",fs.nachname) AS fs_name
		
			FROM 
				`fs_blog_entry` b,
				`fs_foodsaver` fs
		
			WHERE 
				b.foodsaver_id = fs.id
				
			AND
				b.`active` = 1
		
			ORDER BY 
				b.`id` DESC
				
			LIMIT ' . $page . ',10');
	}

	public function listArticle()
	{
		$not = '';
		if (!$this->session->isOrgaTeam()) {
			$not = 'WHERE 		`bezirk_id` IN (' . implode(',', $this->session->listRegionIDs()) . ')';
		}

		return $this->q('
			SELECT 	 	`id`,
						`name`,
						`time`,
						UNIX_TIMESTAMP(`time`) AS time_ts,
						`active`,
						`bezirk_id`
		
			FROM 		`fs_blog_entry`
	
			' . $not . '
	
			ORDER BY `id` DESC');
	}

	public function del_blog_entry($id)
	{
		return $this->del('
			DELETE FROM 	`fs_blog_entry`
			WHERE 			`id` = ' . (int)$id . '
		');
	}

	public function getOne_blog_entry($id)
	{
		$out = $this->qRow('
			SELECT
			`id`,
			`bezirk_id`,
			`foodsaver_id`,
			`active`,
			`name`,
			`teaser`,
			`body`,
			`time`,
			UNIX_TIMESTAMP(`time`) AS time_ts,
			`picture`
			
			FROM 		`fs_blog_entry`
			
			WHERE 		`id` = ' . (int)$id);

		return $out;
	}

	public function add_blog_entry($data)
	{
		$active = 0;
		if ($this->session->isOrgaTeam()) {
			$active = 1;
		} elseif ($this->session->isAdminFor($data['bezirk_id'])) {
			$active = 1;
		}

		$id = $this->insert('
			INSERT INTO 	`fs_blog_entry`
			(
			`bezirk_id`,
			`foodsaver_id`,
			`name`,
			`teaser`,
			`body`,
			`time`,
			`picture`,
			`active`
			)
			VALUES
			(
			' . (int)$data['bezirk_id'] . ',
			' . (int)$data['foodsaver_id'] . ',
			' . $this->strval($data['name']) . ',
			' . $this->strval($data['teaser']) . ',
			"' . $this->safe($data['body']) . '",
			' . $this->dateval($data['time']) . ',
			' . $this->strval($data['picture']) . ',
			' . $active . '
			)');

		$foodsaver = array();
		$orgateam = $this->foodsaverGateway->getOrgateam();
		$botschafter = $this->foodsaverGateway->getBotschafter($data['bezirk_id']);

		foreach ($orgateam as $o) {
			$foodsaver[$o['id']] = $o;
		}
		foreach ($botschafter as $b) {
			$foodsaver[$b['id']] = $b;
		}

		$this->bellGateway->addBell(
			$foodsaver,
			'blog_new_check_title',
			'blog_new_check',
			'fas fa-bullhorn',
			array('href' => '/?page=blog&sub=edit&id=' . $id),
			array(
				'user' => $this->session->user('name'),
				'teaser' => $this->sanitizerService->tt($data['teaser'], 100),
				'title' => $data['name']
			),
			'blog-check-' . $id
		);

		return $id;
	}
}
